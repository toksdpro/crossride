package com.crossover.techtrial.controller;

import com.crossover.techtrial.dto.TopDriverDTO;
import com.crossover.techtrial.model.Ride;
import com.crossover.techtrial.repositories.RideRepository;
import net.minidev.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import com.crossover.techtrial.model.Person;
import com.crossover.techtrial.repositories.PersonRepository;

import java.sql.DatabaseMetaData;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class RideControllerTest {

    MockMvc mockMvc;

    @Mock
    private RideController rideController;

    @Autowired
    private TestRestTemplate template;

    @Autowired
    RideRepository rideRepository;

    @Autowired
    PersonRepository personRepository;

    @Before
    public void setup() throws Exception {
        mockMvc = MockMvcBuilders.standaloneSetup(rideController).build();
    }

    @Test
    public void createNewRide() throws ParseException {
        Person driver = createPerson("driver",
                "driver10000000000001@gmail.com",
                "41DCT",
                LocalDateTime.parse("2018-08-08T12:12:12", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")));

        Person rider = createPerson("rider",
                "rider0000000000001@gmail.com",
                "41DCT",
                LocalDateTime.parse("2018-08-08T12:12:12", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")));

        JSONObject driverParam = new JSONObject();
        driverParam.put("id", driver.getId());
        JSONObject riderParam = new JSONObject();
        riderParam.put("id", rider.getId());
        JSONObject rideParam = new JSONObject();
        rideParam.put("driver", driverParam);
        rideParam.put("rider", riderParam);
        rideParam.put("startTime", "2018-08-08T12:12:12");
        rideParam.put("endTime", "2018-08-08T12:16:12");
        rideParam.put("distance", 30);

        HttpEntity<Object> ride = getHttpEntity(rideParam);
        ResponseEntity<Ride> response = template.postForEntity(
                "/api/ride", ride, Ride.class);
        //Delete this user
        rideRepository.deleteById(response.getBody().getId());
        personRepository.deleteById(driver.getId());
        personRepository.deleteById(rider.getId());

        Assert.assertEquals(driver.getId(), response.getBody().getDriver().getId());
        Assert.assertEquals(200,response.getStatusCode().value());
    }

    @Test
    public void getRideById() throws ParseException {
        Person driver = new Person();
        driver.setEmail("driver10000000000001@gmail.com");
        driver.setName("driver1");
        driver.setRegistrationNumber("41DCT");
        driver.setRegistrationDate(LocalDateTime.parse("2018-08-08T12:12:12", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")));
        driver = personRepository.save(driver);
        Long driverId = driver.getId();
        Person rider = new Person();
        rider.setEmail("rider10000000000001@gmail.com");
        rider.setName("rider1");
        rider.setRegistrationNumber("51DCT");
        rider.setRegistrationDate(LocalDateTime.parse("2018-08-08T12:12:12", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")));
        rider = personRepository.save(rider);
        Long riderId = rider.getId();
        Ride ride = new Ride();
        ride.setDistance(30L);
        ride.setStartTime(LocalDateTime.parse("2018-08-08T12:12:12", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")));
        ride.setEndTime(LocalDateTime.parse("2018-08-08T16:12:12", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")));
        ride.setRider(rider);
        ride.setDriver(driver);
        ride = rideRepository.save(ride);

        Map<String, Long> map = new HashMap<String, Long>();
        map.put("ride-id", ride.getId());
        ResponseEntity<Person> response = template.getForEntity(
                "/api/ride/{ride-id}",  Person.class, map);

        //Delete this user
        rideRepository.deleteById(ride.getId());
        personRepository.deleteById(driverId);
        personRepository.deleteById(riderId);
        Assert.assertEquals(ride.getId(), response.getBody().getId());
        Assert.assertEquals(200,response.getStatusCode().value());

    }

    @Test
    public void getTopDriver() throws ParseException {
        Person driver1 = createPerson("driver1",
                "driver10000000000001@gmail.com",
                "41DCT",
                LocalDateTime.parse("2018-08-08T12:12:12", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")));
        Person driver2 = createPerson("driver2",
                "driver20000000000001@gmail.com",
                "41DCT2",
                LocalDateTime.parse("2018-08-08T12:12:12", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")));
        Person driver3 = createPerson("driver3",
                "driver30000000000001@gmail.com",
                "41DCT3",
                LocalDateTime.parse("2018-08-08T12:12:12", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")));
        Person driver4 = createPerson("driver4",
                "driver40000000000001@gmail.com",
                "41DCT4",
                LocalDateTime.parse("2018-08-08T12:12:12", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")));
        Person driver5 = createPerson("driver5",
                "driver50000000000001@gmail.com",
                "41DCT5",
                LocalDateTime.parse("2018-08-08T12:12:12", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")));
        Person driver6 = createPerson("driver6",
                "driver60000000000001@gmail.com",
                "41DCT6",
                LocalDateTime.parse("2018-08-08T12:12:12", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")));

        Person rider1 = createPerson("driver1",
                "driver10000000000001@gmail.com",
                "41DCT",
                LocalDateTime.parse("2018-08-08T12:12:12", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")));

        Ride ride = createRide(LocalDateTime.parse("2018-08-08T12:12:12", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")),
                LocalDateTime.parse("2018-08-08T16:12:12", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")),
                50L, driver1, rider1);

        Ride ride2 = createRide(LocalDateTime.parse("2018-08-08T13:10:12", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")),
                LocalDateTime.parse("2018-08-08T17:10:10", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")),
                50L, driver2, rider1);

        Ride ride3 = createRide(LocalDateTime.parse("2018-09-08T11:12:12", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")),
                LocalDateTime.parse("2018-09-09T18:12:12", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")),
                50L, driver3, rider1);

        Ride ride4 = createRide(LocalDateTime.parse("2018-08-08T11:12:12", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")),
                LocalDateTime.parse("2018-08-08T20:12:12", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")),
                50L, driver4, rider1);

        Ride ride5 = createRide(LocalDateTime.parse("2018-10-08T10:12:12", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")),
                LocalDateTime.parse("2018-10-08T12:12:12", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")),
                50L, driver4, rider1);

        Ride ride6 = createRide(LocalDateTime.parse("2018-08-08T13:12:12", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")),
                LocalDateTime.parse("2018-08-08T20:12:12", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")),
                50L, driver1, rider1);

        ResponseEntity<TopDriverDTO[]> response = template.getForEntity(
                "/api/top-rides/?startTime=2018-08-08T12:12:12&endTime=2018-10-08T22:12:12",  TopDriverDTO[].class);

        //Delete
        rideRepository.deleteById(ride.getId());
        rideRepository.deleteById(ride2.getId());
        rideRepository.deleteById(ride3.getId());
        rideRepository.deleteById(ride4.getId());
        rideRepository.deleteById(ride5.getId());
        rideRepository.deleteById(ride6.getId());
        personRepository.deleteById(driver1.getId());
        personRepository.deleteById(driver2.getId());
        personRepository.deleteById(driver3.getId());
        personRepository.deleteById(driver4.getId());
        personRepository.deleteById(driver5.getId());
        personRepository.deleteById(driver6.getId());
        personRepository.deleteById(rider1.getId());
        System.out.println(response.getBody().length);
        for (int i=0; i< response.getBody().length; i++) {
            System.out.println(response.getBody()[i].getName() + " " + response.getBody()[1].getEmail()
                    + " " + response.getBody()[i].getMaxRideDurationInSecods() + " " +
                    response.getBody()[i].getTotalRideDurationInSeconds() + " " + response.getBody()[i].getAverageDistance());
        }
        //Assert.assertEquals(ride.getId(), response.getBody().getId());
        Assert.assertEquals(200,response.getStatusCode().value());

    }

    private HttpEntity<Object> getHttpEntity(Object body) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<Object>(body, headers);
    }

    public Person createPerson(String name, String email, String registrationNumber, LocalDateTime registrationDate){
        Person person = new Person();
        person.setEmail(email);
        person.setName(name);
        person.setRegistrationNumber(registrationNumber);
        person.setRegistrationDate(registrationDate);
        return personRepository.save(person);
    }

    public Ride createRide(LocalDateTime start, LocalDateTime end, Long distance, Person driver, Person rider){
        Ride ride = new Ride();
        ride.setStartTime(start);
        ride.setEndTime(end);
        ride.setDistance(distance);
        ride.setDuration(Duration.between(start,end).toMinutes());
        ride.setRider(rider);
        ride.setDriver(driver);
        return rideRepository.save(ride);
    }
}