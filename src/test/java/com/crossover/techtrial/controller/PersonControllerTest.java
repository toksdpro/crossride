/**
 * 
 */
package com.crossover.techtrial.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import com.crossover.techtrial.model.Person;
import com.crossover.techtrial.repositories.PersonRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author kshah
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class PersonControllerTest {
  
  MockMvc mockMvc;
  
  @Mock
  private PersonController personController;
  
  @Autowired
  private TestRestTemplate template;
  
  @Autowired
  PersonRepository personRepository;
  
  @Before
  public void setup() throws Exception {
    mockMvc = MockMvcBuilders.standaloneSetup(personController).build();
  }

  @Test
  public void getAllPersons() throws Exception {
      HttpEntity<Object> person1 = getHttpEntity(
              "{\"name\": \"test 1\", \"email\": \"test10000000000001@gmail.com\","
                      + " \"registrationNumber\": \"41DCT\",\"registrationDate\":\"2018-08-08T12:12:12\" }");
      HttpEntity<Object> person2 = getHttpEntity(
              "{\"name\": \"test 2\", \"email\": \"test20000000000001@gmail.com\","
                      + " \"registrationNumber\": \"41DCT2\",\"registrationDate\":\"2018-08-08T12:12:12\" }");
      ResponseEntity<Person> response1 = template.postForEntity(
              "/api/person", person1, Person.class);
      ResponseEntity<Person> response2 = template.postForEntity(
              "/api/person", person2, Person.class);

      ResponseEntity<Person[]>  response = template.getForEntity(
              "/api/person",  Person[].class);
      //Delete this user
      personRepository.deleteById(response1.getBody().getId());
      personRepository.deleteById(response2.getBody().getId());
      Assert.assertEquals("test 1", response.getBody()[0].getName());
      Assert.assertEquals("test 2", response.getBody()[1].getName());
      Assert.assertEquals(200,response.getStatusCode().value());
  }

  @Test
  public void getPersonById() throws Exception {

      HttpEntity<Object> person = getHttpEntity(
              "{\"name\": \"test 1\", \"email\": \"test10000000000001@gmail.com\","
                      + " \"registrationNumber\": \"41DCT\",\"registrationDate\":\"2018-08-08T12:12:12\" }");
      ResponseEntity<Person> postResponse = template.postForEntity(
              "/api/person", person, Person.class);
      Map<String, Long> map = new HashMap<String, Long>();
      map.put("personId", postResponse.getBody().getId());
      ResponseEntity<Person> getResponse = template.getForEntity(
              "/api/person/{personId}",  Person.class, map);


      //Delete this user
      personRepository.deleteById(getResponse.getBody().getId());
      Assert.assertEquals("test 1", getResponse.getBody().getName());
      Assert.assertEquals(200,getResponse.getStatusCode().value());

  }
  
  @Test
  public void testPanelShouldBeRegistered() throws Exception {
    HttpEntity<Object> person = getHttpEntity(
        "{\"name\": \"test 1\", \"email\": \"test10000000000001@gmail.com\"," 
            + " \"registrationNumber\": \"41DCT\",\"registrationDate\":\"2018-08-08T12:12:12\" }");
    ResponseEntity<Person> response = template.postForEntity(
        "/api/person", person, Person.class);
    //Delete this user
    personRepository.deleteById(response.getBody().getId());
    Assert.assertEquals("test 1", response.getBody().getName());
    Assert.assertEquals(200,response.getStatusCode().value());
  }

  private HttpEntity<Object> getHttpEntity(Object body) {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    return new HttpEntity<Object>(body, headers);
  }

}
